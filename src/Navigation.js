import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './screens/Home';
import Booking from './screens/Booking';

const Stacks = createStackNavigator();

const navigation = () => {
    return (
        <NavigationContainer>
            <Stacks.Navigator>
                <Stacks.Screen name="Gsc Malaysia" component={Home} />
                <Stacks.Screen name="Booking" component={Booking} />
            </Stacks.Navigator>
        </NavigationContainer>
    )
}

export default navigation;