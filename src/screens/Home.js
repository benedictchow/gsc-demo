import React from 'react';
import { useContext } from 'react';
import { StyleSheet, View, Text, FlatList, TouchableOpacity, Image } from 'react-native';
import { MovieContext } from '../service/movieContext';

const Home = props => {

    const { movie, setMovie } = useContext(MovieContext);

    return (
        <View style={styles.container}>
            <FlatList
                data={movie}
                keyExtractor={() => Math.random().toString()}
                renderItem={({ item }) => {
                    return (
                        <TouchableOpacity style={{
                            flex: 1,
                            flexDirection: "row",
                            marginTop: 10,
                            marginBottom: 3,
                            paddingLeft: 10,
                            paddingRight: 10,
                            borderWidth: 0,
                            justifyContent: 'center',
                            backgroundColor: 'white',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 1,
                            },
                            shadowOpacity: 0.20,
                            shadowRadius: 5.00,
                            elevation: 5,
                            paddingTop: 10,
                            paddingBottom: 10
                        }} onPress={() => props.navigation.navigate('Booking', {
                            item
                        })}>
                            <View style={{ flex: 0.3 }}>
                                <Image style={{ width: 100, height: 120, borderRadius: 10 }} source={item.image} />
                            </View>
                            <View style={{ flex: 0.7 }}>
                                <Text numberOfLines={2} style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 5 }}> Title : {item.name}</Text>
                                <Text> description : {item.description}</Text>
                                <Text> Rating : {item.rating}</Text>
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    )
}

export default Home;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10
    }
})