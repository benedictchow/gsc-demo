import React, { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';
import { StyleSheet, View, Text, FlatList, TouchableOpacity, Image, Alert, ToastAndroid } from 'react-native';
import { BookingContext } from '../service/BookingContext';

const Booking = props => {
    const { seat, setSeat } = useContext(BookingContext);
    const [show, setShow] = useState(0);
    const [listItemsRefresh, setListItemsRefresh] = useState(false);

    const params = props.route.params.item;

    function updateSeat(id) {
        let array = [...seat];
        let value = { ...array[params.id] }

        if (value[id - 1].booked == 1) {
            value[id - 1].booked = 2;
            setShow(prev => prev + 1);
        } else {
            value[id - 1].booked = 1;
            setShow(prev => prev - 1);
        }

        array[params.id] = value;
        setListItemsRefresh(!listItemsRefresh);

    }

    function submit() {
        let items = [...seat];
        let item = { ...items[params.id] }

        for (let i = 0; i < Object.keys(item).length; i++) {
            if (item[i].booked == 2) {
                item[i].booked = 3;
                setSeat(items);
                props.navigation.goBack();
            }
        }
    }

    return (
        <View style={{ flex: 1, paddingLeft: 10, paddingRight: 10 }}>
            <View style={{ flex: 1, marginTop: 10 }}>
                <View style={{ paddingLeft: 18, marginBottom: 10 }}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Movie : {params.name} </Text>
                    <View style={{ marginTop: 5, marginBottom: 5, borderWidth: 0.5, width: '100%' }} />
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Seats : </Text>
                    <Text>Red : Booked </Text>
                    <Text>Gray : Available </Text>
                    <Text>Selected : Yellow </Text>
                </View>

                <View style={{ height: 30, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderRadius: 5, marginBottom: 10 }}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Screen</Text>
                </View>

                <FlatList
                    data={seat[params.id]}
                    numColumns={5}
                    extraData={listItemsRefresh}
                    keyExtractor={() => Math.random().toString()}
                    renderItem={({ item }) => {
                        return (
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                {
                                    item.booked == 3 ? (
                                        <TouchableOpacity disabled style={styles.occupied}>
                                            <Image style={{ height: 50, width: 50 }} source={require('../../assets/person.png')} />
                                        </TouchableOpacity>
                                    ) : (
                                        item.booked == 2 ? (
                                            <TouchableOpacity style={styles.selected} onPress={() => updateSeat(item.id)}>
                                                <Image style={{ height: 50, width: 50 }} source={require('../../assets/person.png')} />
                                            </TouchableOpacity>
                                        ) : (
                                            <TouchableOpacity style={styles.available}
                                                onPress={() => updateSeat(item.id)}>
                                                <Image style={{ height: 50, width: 50 }} source={require('../../assets/person.png')} />
                                            </TouchableOpacity>
                                        )
                                    )
                                }
                            </View>

                        )
                    }}
                />
            </View>

            {
                show > 0 ? (
                    <View style={{ height: 50, bottom: 30, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderRadius: 20 }}>
                        <TouchableOpacity onPress={() => submit()}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold' }}> Confirm Tickets ({show})</Text>
                        </TouchableOpacity>
                    </View>
                ) : (
                    null
                )
            }


        </View>
    )
}

const styles = StyleSheet.create({
    occupied: {
        height: 50,
        width: 50,
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        backgroundColor: 'red',
        borderRadius: 20
    },
    selected: {
        height: 50,
        width: 50,
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        backgroundColor: 'yellow',
        borderRadius: 20,
    },
    available: {
        height: 50,
        width: 50,
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        backgroundColor: 'lightgray',
        borderRadius: 20
    }
})

export default Booking;