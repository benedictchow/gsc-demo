import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import Navigation from './src/Navigation';
import { BookingContext } from './src/service/BookingContext';
import { MovieContext } from './src/service/movieContext';

const App = () => {

  const [movie, setMovie] = useState(
    [{
      id: 0,
      name: "Godzilla vs Kong (P13)",
      image: require('./assets/godzilla.png'),
      description: "Good movie",
      rating: 3
    },
    {
      id: 1,
      name: "Tom and Jerry (P13)",
      image: require('./assets/tom.jpeg'),
      description: "Childhood movie",
      rating: 4
    },
    {
      id: 2,
      name: "Hi, Mom (P13)",
      image: require('./assets/mom.jpeg'),
      description: "Touching movie",
      rating: 4
    },
    {
      id: 3,
      name: "Demon Slayer: Kimetsu No Yaiba: Mugen Train (P13)",
      image: require('./assets/demon.jpeg'),
      description: "Best anime movie",
      rating: 4
    }]
  )

  const [seat, setSeat] = useState([
    [
      { id: 1, booked: 1 },
      { id: 2, booked: 1 },
      { id: 3, booked: 1 },
      { id: 4, booked: 3 },
      { id: 5, booked: 3 },
      { id: 6, booked: 1 },
      { id: 7, booked: 1 },
      { id: 8, booked: 1 },
      { id: 9, booked: 1 },
      { id: 10, booked: 1 }
    ],
    [
      { id: 1, booked: 3 },
      { id: 2, booked: 3 },
      { id: 3, booked: 1 },
      { id: 4, booked: 1 },
      { id: 5, booked: 1 },
      { id: 6, booked: 1 },
      { id: 7, booked: 1 },
      { id: 8, booked: 1 },
      { id: 9, booked: 1 },
      { id: 10, booked: 1 }
    ],
    [
      { id: 1, booked: 1 },
      { id: 2, booked: 1 },
      { id: 3, booked: 1 },
      { id: 4, booked: 1 },
      { id: 5, booked: 1 },
      { id: 6, booked: 1 },
      { id: 7, booked: 1 },
      { id: 8, booked: 3 },
      { id: 9, booked: 3 },
      { id: 10, booked: 3 }
    ],
    [
      { id: 1, booked: 1 },
      { id: 2, booked: 1 },
      { id: 3, booked: 1 },
      { id: 4, booked: 1 },
      { id: 5, booked: 1 },
      { id: 6, booked: 3 },
      { id: 7, booked: 3 },
      { id: 8, booked: 3 },
      { id: 9, booked: 1 },
      { id: 10, booked: 1 }
    ]
  ])

  return (
    <MovieContext.Provider value={{ movie, setMovie }}>
      <BookingContext.Provider value={{ seat, setSeat }}>
        <Navigation />
      </BookingContext.Provider>
    </MovieContext.Provider>

  )
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
